<?php

/*
 * Impelements hook_admin_settings()
 */
function daylife_admin_settings() {
  $form['daylife'] = array(
    '#type' => 'fieldset',
    '#title' => t('API keys'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['daylife']['daylife_accesskey'] = array(
    '#type' => 'textfield',
    '#title' => t('Daylife Access key'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => variable_get('daylife_accesskey', ''),
    '#description' => t('Daylife Access Key. Please check at !link page.', array('!link' => l(t('Daylife Developer'), 'http://developer.daylife.com'))),
  );
  $form['daylife']['daylife_sharedsecret'] = array(
    '#type' => 'textfield',
    '#title' => t('Daylife Shared Secret'),
    '#maxlength' => 255,
  	'#required' => TRUE,
    '#default_value' => variable_get('daylife_sharedsecret', ''),
    '#description' => t('Daylife Shared Secret. Please check at !link page.', array('!link' => l(t('Daylife Developer'), 'http://developer.daylife.com'))),
    );
  
  return system_settings_form($form);
}
