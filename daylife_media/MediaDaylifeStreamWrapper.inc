<?php

class MediaDaylifeStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://cache.daylife.com/imageserve/';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'image/daylife';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    return 'http://cache.daylife.com/imageserve/'. $this->getImageID() .'/100x100.jpg';
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-daylife/' . $this->getImageID() . '.100x100.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
  
	public function setUri($uri) {
    $this->uri = $uri;
  }
  
  public function getImageID() {
    if ($url = parse_url($this->uri)) {
      if ($url['scheme'] == 'daylife' && is_numeric($url['host'])) {
        return $url['host'];
      }
    }

    return NULL;
  }
}
