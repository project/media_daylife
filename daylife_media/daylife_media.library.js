(function ($) {
  Drupal.daylifeLibrary = Drupal.daylifeLibrary || {};
  Drupal.daylifeLibrary.library = Drupal.daylifeLibrary.library || {};
  Drupal.daylifeLibrary.loaded = Drupal.daylifeLibrary.loaded || false;

  Drupal.behaviors.daylifeLibrary = {
    attach: function (context, settings) {

      // Check if object already exists
      if (typeof Drupal.daylifeLibrary.library.start != 'function') {
        Drupal.daylifeLibrary.library = new Drupal.media.browser.library(Drupal.settings.media.browser.daylife);
      }

      $('#media-browser-tabset').not('.dl-processed').addClass('dl-processed').bind('tabsselect', function (event, ui) {
        if (ui.tab.hash === '#media-tab-daylife') {
          // Prevent reloading of media list on tabselect if already loaded in media list
          if (!Drupal.daylifeLibrary.loaded) {
            var params = {};
            for (var p in Drupal.settings.media.browser.daylife) {
              params[p] = Drupal.settings.media.browser.library[p];
            }

            Drupal.daylifeLibrary.library.start($(ui.panel), params);
            $('#scrollbox').bind('scroll', Drupal.daylifeLibrary.library, Drupal.daylifeLibrary.library.scrollUpdater);
            Drupal.daylifeLibrary.loaded = true;
          }
        }
      });

      $('#edit-filter--2').not('.dl-processed').addClass('dl-processed').click(function(ev) {
        ev.preventDefault();

        // Acquire filter form values
        var keywordsVal = $(Drupal.daylifeLibrary.library.renderElement).find('#edit-title--2').val();
        // set library object parameters (used for ajax loading new media into list)
        Drupal.daylifeLibrary.library.params.filter = {keywords: keywordsVal};

        // Remove the media list
        Drupal.daylifeLibrary.library.cursor = 0;
        Drupal.daylifeLibrary.library.mediaFiles = [];
        $(Drupal.daylifeLibrary.library.renderElement).find('#media-browser-library-list li').remove();
        $('#scrollbox').unbind('scroll').bind('scroll', Drupal.daylifeLibrary.library, Drupal.daylifeLibrary.library.scrollUpdater);

        // Set a flag so we don't make multiple concurrent AJAX calls
        Drupal.daylifeLibrary.library.loading = true;
        // Reload the media list
        Drupal.daylifeLibrary.library.loadMedia();
      });

      $('#edit-reset--2').not('.dl-processed').addClass('dl-processed').click(function(ev) {
        ev.preventDefault();

        // Reset filter form values
        delete Drupal.daylifeLibrary.library.params.filter;
        $(Drupal.daylifeLibrary.library.renderElement).find('#edit-title--2').val('');
        $('#scrollbox').unbind('scroll').bind('scroll', Drupal.daylifeLibrary.library, Drupal.daylifeLibrary.library.scrollUpdater);

        // Remove the media list
        Drupal.daylifeLibrary.library.cursor = 0;
        Drupal.daylifeLibrary.library.mediaFiles = [];
        $(Drupal.daylifeLibrary.library.renderElement).find('#media-browser-library-list li').remove();
        // Set a flag so we don't make multiple concurrent AJAX calls
        Drupal.daylifeLibrary.library.loading = true;
        // Reload the media list
        Drupal.daylifeLibrary.library.loadMedia();
      });
    }
  };

  Drupal.ajax.prototype.commands.daylife_media_upload = function (ajax, response, status) {
    Drupal.daylifeLibrary.library.mediaSelected([response.data]);
    $(Drupal.daylifeLibrary.library.renderElement).find('.fake-ok').trigger('click');
  };
})(jQuery);
